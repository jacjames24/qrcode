//
//  HistoryCell.swift
//  QRCode
//
//  Created by Jacquiline Guzman on 2017/09/28.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

class HistoryCell: UICollectionViewCell {

    var dateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm"
        return formatter
    }()

    var scannedItem: Scanned? {
        didSet {
            if let content = scannedItem?.content {
                contentLabel.text = content
            }

            if let date = scannedItem?.dateCreated {
                dateCreatedLabel.text = dateFormatter.string(from: date)
            }
        }
    }

    var isInEditMode: Bool? {
        didSet {
            if isInEditMode != nil {
                deleteItemButton.isHidden = isInEditMode == true ? false : true
            }
        }
    }

    let contentLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(18)
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 1
        label.text = "placeholder"
        return label
    }()

    let dateCreatedLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(13)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.text = "placeholder"
        return label
    }()

    let deleteItemButton: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "itemdelete")
        button.setImage(image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), for: UIControl.State())
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = true
        return button
    }()

    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)

        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupViews() {
        addSubview(contentLabel)
        addSubview(dateCreatedLabel)
        addSubview(deleteItemButton)
        addSubview(separatorView)

        addConstraintsWithFormat(format: "V:|-14-[v0(25)]-1-[v1(25)]-14-[v2(1)]|", views: contentLabel, dateCreatedLabel, separatorView)

        addConstraintsWithFormat(format: "H:|-16-[v0]-32-|", views: contentLabel)
        addConstraintsWithFormat(format: "H:|-16-[v0]-32-|", views: dateCreatedLabel)
        addConstraintsWithFormat(format: "H:|[v0]|", views: separatorView)

        deleteItemButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        deleteItemButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
