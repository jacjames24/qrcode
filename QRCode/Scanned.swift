//
//  Scanned.swift
//  QRCode
//
//  Created by Jacquiline Guzman on 2017/09/28.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import RealmSwift

class Scanned: Object {
    @objc dynamic var content = ""
    @objc dynamic var dateCreated: Date?
}
