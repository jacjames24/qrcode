//
//  QRTabbarController.swift
//  QRCode
//
//  Created by Jacquiline Guzman on 2017/09/28.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

class QRTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let scanViewController = ScanViewController()
        scanViewController.tabBarItem = UITabBarItem(title: "Scan", image: UIImage(named: "qrcode"), tag: 1)

        let historyViewController = HistoryViewController(collectionViewLayout: UICollectionViewFlowLayout())
        historyViewController.tabBarItem = UITabBarItem(title: "History", image: UIImage(named: "clock"), tag: 2)

        let viewControllerList = [scanViewController, historyViewController]

        viewControllers = viewControllerList.map {
            UINavigationController(rootViewController: $0)
        }
    }
}
