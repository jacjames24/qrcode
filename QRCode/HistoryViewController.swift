//
//  HistoryViewController.swift
//  QRCode
//
//  Created by Jacquiline Guzman on 2017/09/28.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit
import RealmSwift

class HistoryViewController: UICollectionViewController {

    let cellId = "cellId"
    var scannedItems: Results<Scanned>?
    var isInEditMode = false
    var deleteBarButtonItem: UIBarButtonItem?

    func fetchScannedItems() {
        DataService.sharedInstance.getScannedItems { (scannedItems: Results<Scanned>) in
            self.scannedItems = scannedItems

            guard let collectionView = self.collectionView else { return }
            collectionView.reloadDataOnMainQueue()
        }
    }

    func clearAllItems() {
        DataService.sharedInstance.clearAllItems()

        guard let collectionView = self.collectionView else { return }
        collectionView.reloadDataOnMainQueue()
    }

    func deleteSingleItem(scanned: Scanned) {
        DataService.sharedInstance.deleteItem(scanned: scanned)

        guard let collectionView = self.collectionView else { return }
        collectionView.reloadDataOnMainQueue()
    }

    func setupNavigationBarItems() {
        title = "History"

        let deleteAllButton: UIButton = {
            let deleteAllButton = UIButton(type: .system)
            deleteAllButton.setImage(UIImage(named: "delete"), for: .normal)
            deleteAllButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            deleteAllButton.addTarget(self, action: #selector(askUserForConfirmationForDelete), for: .touchUpInside)
            return deleteAllButton
        }()

        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: deleteAllButton)

        deleteBarButtonItem = UIBarButtonItem(
            title: "Edit",
            style: UIBarButtonItem.Style.plain,
            target: self,
            action: #selector(toggleEditMode))
        navigationItem.rightBarButtonItem = deleteBarButtonItem
    }

    @objc func askUserForConfirmationForDelete() {
        showAlert(
            title: "Clear All",
            message: "Clear all history?",
            handlerForYesAction: { (_: UIAlertAction!) in self.clearAllItems() })
    }

    func askForConfirmationForDeleteSingleItem(item: Scanned) {
        showAlert(
            title: "Delete Item",
            message: "Do you want to delete this item?",
            handlerForYesAction: { (_: UIAlertAction!) in self.deleteSingleItem(scanned: item) })
    }

    @objc func toggleEditMode() {
        guard let deleteBarButtonItem = self.deleteBarButtonItem else { return }

        deleteBarButtonItem.title = deleteBarButtonItem.title == "Edit" ? "Done" : "Edit"
        isInEditMode = !isInEditMode

        guard let collectionView = self.collectionView else { return }
        collectionView.reloadDataOnMainQueue()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBarItems()

        guard let collectionView = self.collectionView else { return }
        collectionView.backgroundColor = UIColor.white
        collectionView.register(HistoryCell.self, forCellWithReuseIdentifier: cellId)

        fetchScannedItems()
    }

    override func viewWillAppear(_ animated: Bool) {
        fetchScannedItems()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let scannedItems = scannedItems else { return 0 }

        return scannedItems.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? HistoryCell else {
            return UICollectionViewCell()
        }

        guard let scannedItems = scannedItems else { return cell }
        cell.scannedItem = scannedItems[indexPath.item]
        cell.isInEditMode = isInEditMode

        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let scannedItems = scannedItems else { return }

        if isInEditMode {
            askForConfirmationForDeleteSingleItem(item: scannedItems[indexPath.item])
            return
        }

        displayDetails(scannedCode: scannedItems[indexPath.item].content)
    }
}

extension HistoryViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: view.frame.width, height: 80)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

}
