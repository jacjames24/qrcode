//
//  File.swift
//  QRCode
//
//  Created by Jacquiline Guzman on 2017/09/28.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

extension UIView {

    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()

        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }

        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format,
                                                      options: NSLayoutConstraint.FormatOptions(),
                                                      metrics: nil,
                                                      views: viewsDictionary))
    }
}

extension UIViewController {
    func showAlert(title: String, message: String,
                   handlerForYesAction: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .default,
                                      handler: handlerForYesAction))

        alert.addAction(UIAlertAction(title: "No", style: .cancel,
                                      handler: { (_: UIAlertAction!) in }))

        self.present(alert, animated: true, completion: nil)
    }

    //was placed here because details are being used by both scan and history tabs
    func displayDetails(scannedCode: String) {
        let _:DetailsViewController = {
            let detailsVC = DetailsViewController()
            detailsVC.scannedString = scannedCode
            self.navigationController?.pushViewController(detailsVC, animated: true)
            return detailsVC
        }()
    }
}

extension UIButton {
    func enableButton(target: Any, title: String, action: Selector) {
        self.isHidden = false
        self.setTitle(title, for: UIControl.State())
        self.addTarget(target, action: action, for: .touchUpInside)
    }
}

extension UICollectionView {
    func reloadDataOnMainQueue() {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}
