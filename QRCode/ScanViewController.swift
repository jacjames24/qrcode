//
//  ScanViewController.swift
//  QRCode
//
//  Created by Jacquiline Guzman on 2017/09/28.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit
import AVFoundation
import RealmSwift

class ScanViewController: UIViewController {

    var captureSession: AVCaptureSession?
    var captureDevice: AVCaptureDevice?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?

    // the box around the detected qr code
    let codeFrame: UIView = {
        let codeFrame = UIView()
        codeFrame.layer.borderColor = UIColor.green.cgColor
        codeFrame.layer.borderWidth = 2
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        return codeFrame
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Scan"
        setupCaptureDeviceAndSession()
    }

    override func viewWillAppear(_ animated: Bool) {
        // for scenarios where user goes back from the detail screen to scanview
        if captureDevice == nil { return }

        if let captureSession = captureSession, !captureSession.isRunning {
            codeFrame.frame = CGRect.zero
            captureSession.startRunning()
        }
    }

    fileprivate func setupCaptureDeviceAndSession() {
        // Do any additional setup after loading the view, typically from a nib.

        captureDevice = AVCaptureDevice.default(for: AVMediaType(rawValue: convertFromAVMediaType(AVMediaType.video)))

        do {
            let input = try AVCaptureDeviceInput.init(device: captureDevice!)
            captureSession = AVCaptureSession()

            guard let captureSession = captureSession else { return }

            captureSession.addInput(input)

            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]

            captureSession.startRunning()

            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)

            guard let videoPreviewLayer = videoPreviewLayer else { return }

            videoPreviewLayer.videoGravity = AVLayerVideoGravity(rawValue: convertFromCALayerContentsGravity(CALayerContentsGravity.resizeAspectFill))
            videoPreviewLayer.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer)
        } catch {
            print("Error in device input")
        }

    }

    fileprivate func addToHistory(scannedCode: String) {
        let scanned = Scanned()
        scanned.content = scannedCode
        scanned.dateCreated = Date()

        DataService.sharedInstance.addScannedItem(scanned: scanned)
    }

}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromAVMediaType(_ input: AVMediaType) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromAVMetadataObjectObjectType(_ input: AVMetadataObject.ObjectType) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromCALayerContentsGravity(_ input: CALayerContentsGravity) -> String {
	return input.rawValue
}

extension ScanViewController: AVCaptureMetadataOutputObjectsDelegate {

    func metadataOutput(_ captureOutput: AVCaptureMetadataOutput,
                        didOutput metadataObjects: [AVMetadataObject],
                        from connection: AVCaptureConnection) {

        if metadataObjects.count == 0 { return }

        guard let metadataObject = metadataObjects[0] as? AVMetadataMachineReadableCodeObject else { return }
        guard let stringCodeValue = metadataObject.stringValue else { return }

        print("the string code value = \(stringCodeValue)")

        view.addSubview(codeFrame)

        guard let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObject) else { return }
        codeFrame.frame = barCodeObject.bounds

        let systemSoundId: SystemSoundID = kSystemSoundID_Vibrate
        AudioServicesPlayAlertSound(systemSoundId)

        guard let captureSession = captureSession else { return }
        captureSession.stopRunning()

        addToHistory(scannedCode: stringCodeValue)
        displayDetails(scannedCode: stringCodeValue)
    }

}
