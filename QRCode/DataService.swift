//
//  DataService.swift
//  QRCode
//
//  Created by Jacquiline Guzman on 2017/09/29.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

// we are abstracting the database functionalities within this class so that
// in the event that we decide to use a different database, we can easily do so within
// this service without majorly affecting the view controllers

import UIKit
import RealmSwift

class DataService: NSObject {

    static let sharedInstance = DataService()

    func addScannedItem(scanned: Scanned) {
        do {
            let realm = try Realm()

            try realm.write {
                realm.add(scanned)
            }

        } catch {
            print("there was an issue saving the scanned item")
        }
    }

    func getScannedItems(completion: (Results<Scanned>) -> Void) {
        do {
            let realm = try Realm()
            var scannedItems: Results<Scanned>

            scannedItems = realm.objects(Scanned.self).sorted(byKeyPath: "dateCreated", ascending: false)
            completion(scannedItems)

        } catch {
            print("there was an issue fetching the scanned items")
        }
    }

    func clearAllItems() {
        do {
            let realm = try Realm()

            try realm.write {
                realm.deleteAll()
            }
        } catch {
            print("there was an issue deleting all items")
        }
    }

    func deleteItem(scanned: Scanned) {
        do {
            let realm = try Realm()

            try realm.write {
                realm.delete(scanned)
            }

        } catch {
            print("there was an issue deleting the item")
        }
    }

}
