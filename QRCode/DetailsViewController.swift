//
//  DetailsViewController.swift
//  QRCode
//
//  Created by Jacquiline Guzman on 2017/09/28.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit
import MessageUI

class DetailsViewController: UIViewController {
    var scannedString: String? {
        didSet {
            guard let scannedString = scannedString else { return }

            codeLabel.text = scannedString
            interpretQRCode(scannedString: scannedString)
        }
    }

    var mailComposer = MFMailComposeViewController()

    let label: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.text = "Scanned String from QR Code:"
        return label
    }()

    let codeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20)
        label.numberOfLines = 3
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        return label
     }()

    let actionButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Action", for: UIControl.State())
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = true
        return button
    }()

    override func viewDidLoad() {
        view.backgroundColor = UIColor.white
        title = "QR Code"

        setupViews()
        mailComposer.mailComposeDelegate = self
    }

    fileprivate func setupViews() {

        // actual scanned string - this will be centered vertically
        view.addSubview(codeLabel)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: codeLabel)
        codeLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

        // label's position is then going to be relative to the scanned string label
        view.addSubview(label)
        view.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: label)
        label.bottomAnchor.constraint(equalTo: codeLabel.topAnchor, constant: -30).isActive = true

        // action button's position is relative to scanned string label
        view.addSubview(actionButton)
        actionButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        actionButton.topAnchor.constraint(equalTo: codeLabel.bottomAnchor, constant: 50).isActive = true
        actionButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        actionButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
    }

    // for now we will support URL, email, and dialer
    fileprivate func interpretQRCode(scannedString: String) {
        if scannedString.hasPrefix("http://") || scannedString.hasPrefix("https://") {
            actionButton.enableButton(target: self, title: "Go to this URL", action: #selector(openURL))
            return
        }

        if scannedString.hasPrefix("mailto:") {
            actionButton.enableButton(target: self, title: "Send email", action: #selector(openMailApp))
            return
        }

        if scannedString.hasPrefix("tel:") {
            actionButton.enableButton(target: self, title: "Call", action: #selector(openDialer))
        }
    }

    @objc fileprivate func openURL() {
        guard let scannedString = scannedString else { return }

        if let url = URL(string: scannedString), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    @objc fileprivate func openMailApp() {
        if !MFMailComposeViewController.canSendMail() { return }

        guard let scannedString = scannedString else { return }
        mailComposer.setToRecipients([scannedString.replacingOccurrences(of: "mailto:", with: "", options: .regularExpression)])
        present(mailComposer, animated: true, completion: nil)
    }

    @objc fileprivate func openDialer() {
        guard let scannedString = scannedString else { return }
        let number = scannedString.replacingOccurrences(of: "tel:", with: "tel://", options: .regularExpression)

        if let url = URL(string: number), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
private func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

extension DetailsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}
